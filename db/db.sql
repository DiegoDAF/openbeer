create table beers (
    id serial not null,
    barcode bigint not null primary key,
    beername varchar(100) not null,
    created timestamp not null default now()
);
create table attributs (
    id serial not null primary key,
    attributename varchar(100),
    created timestamp not null default now() 
);
create table beerattributes
(
    id serial not null,
    beerid integer NOT NULL,
    attributeid integer NOT NULL,
    valor character varying(100) COLLATE pg_catalog."default",
    created timestamp without time zone NOT NULL DEFAULT now(),
    CONSTRAINT beerattributes_pkey PRIMARY KEY (beerid, attributeid)
);

CREATE TABLE IF NOT EXISTS public.beerimages
(
    id serial NOT NULL,
    beerid bigint NOT NULL,
    imge bytea NOT NULL,
    CONSTRAINT beerimages_pkey PRIMARY KEY (beerid, id)
);

create table marcas(
    id serial not null,
    cuit bigint not null primary key,
    nombre varchar not null,
    created timestamp without time zone NOT NULL DEFAULT now()
);
