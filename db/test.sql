EXPLAIN ANALYZE VERBOSE
SELECT id, barcode, beername, created
FROM public.beers
where beername ilike '%amber%'
WHERE to_tsvector('english', beername) @@ to_tsquery('english', 'amber');
    ;
insert into marcas(cuit, nombre)    
select 30715385666, 'La Yungeré S. A.'
returning id ;
    
insert into public.beers (barcode, beername) 
select 7798339251134, 'Artesanal Pampa Brewing Co Cerveza Amber Ale'
returning id ;



CREATE INDEX beers_ts_idx ON beers USING GIN (to_tsvector('english', beername));

SELECT id, attributename, created
FROM public.attributs;
    
SELECT id, beerid, attributeid, valor, created
FROM public.beerattributes;



insert into public.attributs(attributename)
--select 'Tipo de fermentación' -- ALE LAGER
-- select 'Intensidad' 1 a 5
select 'Nacionalidad' 
returning id ;

SELECT id, beerid, attributeid, valor, created
FROM public.beerattributes;

insert into public.beerattributes (beerid, attributeid, valor)
select 1, 6, 'Argentina'
--select 1, 4, '22'
--select 1, 3, '4.9'
--select 1, 2, '20'
--select 1, 1, 'Ale'
returning id ;


select * from beers